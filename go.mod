module git.lubar.me/ben/ipfs-fuse

go 1.13

require (
	github.com/hanwen/go-fuse/v2 v2.0.2
	github.com/ipfs/go-ipfs-api v0.0.3-0.20191220145807-5d34b6b77a49
)
